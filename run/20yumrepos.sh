#!/bin/bash

# Link repositories.
for f in "$RESDIR"/yumrepos/*.repo; do
    if [ -f $f ]; then
        REPO=/etc/yum.repos.d/"${f##*/}"
        if [ ! -f "$REPO" ]; then
            ln -s "$f" "$REPO"
        fi
    fi
done

for f in "$RESDIR"/yumrepos/*.repo."$OSNICK"; do
    if [ -f $f ]; then
        REPO=/etc/yum.repos.d/"${f##*/}"
        REPO="${REPO%.$OSNICK}"
        if [ ! -f "$REPO" ]; then
            ln -s "$f" "$REPO"
        fi
    fi
done

# Install repository configuration packages.
rpm -ivh http://remi.conetix.com.au/enterprise/remi-release-6.rpm
yum-config-manager --enable remi-php70
rpm -ivh http://www.percona.com/downloads/percona-release/redhat/0.1-3/percona-release-0.1-3.noarch.rpm
