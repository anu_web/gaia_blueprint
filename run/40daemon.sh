#!/bin/bash

if [ ! -d /var/gaia ]; then
    mkdir /var/gaia
    mkdir /var/gaia/misc
    chown daemon:daemon /var/gaia/misc
fi
