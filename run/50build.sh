#!/bin/bash

for f in "$RESDIR"/build/"$SERVER"/*.sh; do
    if [ -f $f ]; then
        . "$f"
    fi
done
