Gaia hosting
============

The Gaia hosting cluster is a high-availability, high-performance hosting environment optimised for running Drupal sites on multiple codebases (platforms). There are four server roles in play:

- Master: the main provisioning server running Aegir.
- Proxy: reverse proxy servers running Varnish to cache backend pages.
- Web: backend web servers running the application, i.e. Drupal.
- Database: load-balanced database servers running MariaDB Galera Cluster with multi-master replication.


Contents
--------

The `build.sh` script in the same directory as this README is the main build script. The other directories contain the following contents:

- `check/`: pre-build sanity checks.
- `build/`: build scripts for specific servers.

Files contained under the `check` directory are service configuration files necessary to preconfigured to run the services specific to a server role.

Files contained under the `build` directory are scripts that can be used to provision a server from the base operating system installation.
