#!/bin/bash

INSTALL_PATH=/usr/share/gaia_blueprint
BLUEPRINT_REPO=https://bitbucket.org/anu_web/gaia_blueprint.git

if [ $(whoami) != root ]; then
    echo "This script must be run as root." 1>&2
    exit 1
fi

if [ -z $(which yum 2>/dev/null) ]; then
    echo "This script requires yum to run." 1>&2
    exit 1
fi

if [ ! -d $INSTALL_PATH ]; then
    yum -y install git
    git clone "$BLUEPRINT_REPO" "$INSTALL_PATH"
fi

sh "$INSTALL_PATH"/build.sh $@
