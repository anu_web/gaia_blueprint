#!/bin/bash

if [ $(whoami) != root ]; then
    echo "The script must be run as root." 1>&2
    exit 1
fi
