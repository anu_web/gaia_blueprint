#!/bin/bash

export OS=$(lsb_release -si)
export ARCH=$(uname -m)
export VER=$(lsb_release -sr)

if [ "$ARCH" != x86_64 ]; then
    echo "Only 64-bit systems are supported." 1>&2
    exit 1
fi

if [ "$OS" = RedHatEnterpriseServer ] && [[ "$VER" = 6.* ]]; then
    OSNICK=rhel6
elif [ "$OS" = CentOS ] && [[ "$VER" = 6.* ]]; then
    OSNICK=centos6
else
    echo "Unsupported operating system." 1>&2
    exit 1
fi
export OSNICK
