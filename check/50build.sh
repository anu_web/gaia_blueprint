#!/bin/bash

if [ -z "$SERVER" ]; then
    echo 'The server configuration name is missing.' 1>&2
    exit 1
fi

if [ ! -d "$RESDIR/build/$SERVER" ]; then
    echo "The specified configuration '$SERVER' does not exist." 1>&2
    exit 1
fi

if [ -z "$DOMAIN" ]; then
    echo 'The domain name is missing.' 1>&2
    exit 1
fi
