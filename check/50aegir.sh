#!/bin/bash

# TODO Change to setup option.
export AEGIR_UID=511
export AEGIR_GID=511

LOOKUP_USER=$(getent passwd $AEGIR_UID)
if [ -n "$LOOKUP_USER" ] && [[ $LOOKUP_USER != aegir:* ]]; then
    echo "A user with the id $AEGIR_UID already exists and is not aegir." 1>&2
    exit 1
fi

LOOKUP_GROUP=$(getent group $AEGIR_GID)
if [ -n "$LOOKUP_GROUP" ] && [[ $LOOKUP_GROUP != aegir:* ]]; then
    echo "A group with the id $AEGIR_GID already exists and is not aegir." 1>&2
    exit 1
fi
