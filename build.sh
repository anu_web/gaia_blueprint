#!/bin/bash

# Prepare variables.
export COMMAND=$(readlink -f $0)
export SERVER="$1"
export DOMAIN="$2"
export BASEDIR=$(dirname "$COMMAND")
export RESDIR="$BASEDIR/resources"

# Perform pre-command checks.
for f in $BASEDIR/check/*.sh; do
    . $f
done

# Run build scripts.
for f in $BASEDIR/run/*.sh; do
    . $f
done

