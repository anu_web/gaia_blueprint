#!/bin/bash

# Set up Varnish.
rpm --nosignature -i https://repo.varnish-cache.org/redhat/varnish-3.0.el6.rpm
yum -y install varnish

if [ ! -f /etc/sysconfig/varnish.stock ]; then
    mv /etc/sysconfig/varnish /etc/sysconfig/varnish.stock
    ln -s "$RESDIR/varnish/gaia" /etc/sysconfig/varnish
fi
if [ ! -d /etc/varnish/gaia ]; then
    ln -s "$RESDIR/varnish/vcl" /etc/varnish/gaia
fi
if [ ! -d /etc/varnish/gaia.vcl ]; then
    VCL=default.vcl
    if [[ $DOMAIN == *"-dev.anu.edu.au" ]]; then
        VCL=dev.vcl
    fi
    ln -s "$RESDIR/varnish/$VCL" /etc/varnish/gaia.vcl
fi

chkconfig varnish on
chkconfig varnishncsa on
service varnish start
service varnishncsa start

# Set up SElinux.
checkmodule -M -m -o /tmp/varnishlnkfile.mod "$RESDIR/selinux/varnishlnkfile.te"
semodule_package -o /tmp/varnishlnkfile.pp -m /tmp/varnishlnkfile.mod
semodule -i /tmp/varnishlnkfile.pp
semanage permissive -a varnishd_t
