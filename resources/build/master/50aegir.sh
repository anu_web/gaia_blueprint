#!/bin/bash

export AEGIR_VERSION=7.x
export AEGIR_ROOT_USER=aegir_root
export AEGIR_FRONTEND_USER=superuser

yum -y install nginx postfix redis rsync clamav clamd unzip
yum -y install php php-fpm php-cli php-common php-gd php-mysql php-pear php-process php-opcache php-ldap php-xml \
    php-pecl-apcu php-pecl-zip php-mbstring

semanage permissive -a httpd_t

# Prepare Aegir user.
groupadd -g $AEGIR_GID aegir
useradd --home-dir /var/aegir aegir -u $AEGIR_UID -g aegir
gpasswd -a aegir nginx
chmod -R 755 /var/aegir
if [ ! -f /etc/sudoers.d/aegir ]; then
    ln -s "$RESDIR/sudoers/aegir" /etc/sudoers.d/aegir
fi

# Set up web services.
service redis start
chkconfig redis on
service nginx start
chkconfig nginx on
service postfix start
chkconfig postfix on
if [ ! -f /etc/nginx/nginx.conf.stock ]; then
    mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.stock
    ln -s "$RESDIR/nginx/nginx.conf" /etc/nginx/nginx.conf
fi
if [ ! -f /etc/nginx/conf.d/aegir.conf ]; then
    ln -s /var/aegir/config/nginx.conf /etc/nginx/conf.d/aegir.conf
fi
if [ ! -d /var/lib/nginx ]; then
    mkdir /var/lib/nginx
fi
if [ ! -d /var/lib/php/opcache-nginx ]; then
    mkdir /var/lib/php/opcache-nginx
    chgrp nginx /var/lib/php/opcache-nginx
    chmod g+w /var/lib/php/opcache-nginx
fi

if [ ! -f /etc/php.d/gaia.ini ]; then
    ln -s "$RESDIR/php/gaia.ini" /etc/php.d/gaia.ini
fi
if [ -f /etc/php-fpm.d/www.conf ]; then
    echo "; Gaia override" >> /etc/php-fpm.d/www.conf
    echo "user = nginx" >> /etc/php-fpm.d/www.conf
    echo "group = nginx" >> /etc/php-fpm.d/www.conf
fi
service php-fpm start
chkconfig php-fpm on
service clamd start
chkconfig clamd on

# Set up MariaDB.
. "$RESDIR/build_tasks/mariadb.sh"
service mysql start
chkconfig mysql on
. "$RESDIR/build_tasks/aegir_root.sh"
echo -e "\nRunning mysql_secure_installation...\n"
/usr/bin/mysql_secure_installation

# Set up Drush.
yum -y --enablerepo=remi install drush
if [ $(which drush) != /usr/local/bin/drush ]; then
    ln -s "$(which drush)" /usr/local/bin/drush
fi

# Install Aegir.
CMD="drush dl --destination=/var/aegir/.drush provision-$AEGIR_VERSION
drush cache-clear drush
drush hostmaster-install -y --aegir_host=$DOMAIN --aegir_db_host=$DOMAIN --aegir_db_user=$(printf "%q" "$AEGIR_ROOT_USER") --aegir_db_pass=$(printf "%q" "$AEGIR_ROOT_PASS") --http_service_type=nginx --web_group=nginx $DOMAIN
drush @hostmaster -y pm-enable hosting_queued"
su -s /bin/bash -c "$CMD" - aegir
if [ $? != 0 ]; then
    exit 1
fi
service nginx reload

if [ ! -f /etc/init.d/hosting-queued ]; then
    ln -s "$RESDIR/init/hosting-queued" /etc/init.d/hosting-queued
    chkconfig --add hosting-queued
fi
chmod 755 /etc/init.d/hosting-queued
service hosting-queued status > /dev/null || service hosting-queued start
if [ ! -f /etc/init.d/hosting-check.sh ]; then
    chmod 755 "$RESDIR/cron/hosting-check.sh"
    ln -s "$RESDIR/cron/hosting-check.sh" /etc/cron.daily/hosting-check.sh
fi

# Prompt Aegir frontend password change.
read -s -p "Set password for Aegir frontend: " AEGIR_FRONTEND_PASS; echo
CMD="drush @hostmaster sql-query \"UPDATE users SET name='$(printf "%q" "$AEGIR_FRONTEND_USER")' WHERE uid=1\"
drush @hostmaster user-password $(printf "%q" "$AEGIR_FRONTEND_USER") --password=$(printf "%q" "$AEGIR_FRONTEND_PASS")"
su -s /bin/bash -c "$CMD" - aegir
if [ $? -eq 0 ]; then
    echo "You can now login via http://$DOMAIN/ using the username '$AEGIR_FRONTEND_USER' and the entered password."
fi
AEGIR_FRONTEND_PASS=""
CMD=""

# Set up SELinux.
setsebool -P rsync_client 1
semanage permissive -a rsync_t
