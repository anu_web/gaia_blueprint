#!/bin/bash

yum -y install unzip

. "$RESDIR/build_tasks/mariadb.sh"
if [ -f "$RESDIR/mariadb/gaia_cluster.cnf" ] && [ ! -f "/etc/my.cnf.d/gaia_cluster".cnf ]; then
    ln -s "$RESDIR/mariadb/gaia_cluster.cnf" "/etc/my.cnf.d/gaia_cluster.cnf"
fi
chkconfig mysql off

# Set up service.
read -p "Do you want to bootstrap a new cluster? [y/N] " BOOTSTRAP_CLUSTER; echo
if [ "$BOOTSTRAP_CLUSTER" == 'y' ]; then
    service mysql bootstrap
    mysql -uroot -e"GRANT RELOAD, LOCK TABLES, REPLICATION CLIENT ON *.* TO 'galera'@'%' IDENTIFIED BY 'galera';"
    . "$RESDIR/build_tasks/aegir_root.sh"
    echo -e "\nRunning mysql_secure_installation...\n"
    /usr/bin/mysql_secure_installation
else
    read -p "Do you want to join the existing cluster? [y/N] " JOIN_CLUSTER; echo
    if [ "$JOIN_CLUSTER" == 'y' ]; then
        service mysql start
    fi
fi
