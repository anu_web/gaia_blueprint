#!/bin/bash

# Set up essential packages.
yum -y install nginx postfix redis rsync clamav clamd unzip
yum -y install php php-fpm php-cli php-common php-gd php-mysql php-pear php-process php-opcache php-ldap php-xml php-mcrypt\
    php-pecl-apcu php-pecl-zip php-mbstring

semanage permissive -a httpd_t

# Prepare Aegir user.
groupadd -g $AEGIR_GID aegir
useradd --home-dir /var/aegir aegir -u $AEGIR_UID -g aegir
gpasswd -a aegir nginx
chmod -R 755 /var/aegir
if [ ! -f /etc/sudoers.d/aegir ]; then
    ln -s "$RESDIR/sudoers/aegir" /etc/sudoers.d/aegir
fi

# Set up SELinux.
checkmodule -M -m -o /tmp/sshd_var.mod "$RESDIR/selinux/sshd_var.te"
semodule_package -o /tmp/sshd_var.pp -m /tmp/sshd_var.mod
semodule -i /tmp/sshd_var.pp

# Set up web services.
service redis start
chkconfig redis on
service nginx start
chkconfig nginx on
service postfix start
chkconfig postfix on
if [ ! -f /etc/nginx/nginx.conf.stock ]; then
    mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.stock
    ln -s "$RESDIR/nginx/nginx.conf" /etc/nginx/nginx.conf
fi
if [ ! -f /etc/nginx/conf.d/aegir.conf ]; then
    ln -s /var/aegir/config/nginx.conf /etc/nginx/conf.d/aegir.conf
fi
if [ ! -d /var/lib/nginx ]; then
    mkdir /var/lib/nginx
fi
if [ ! -d /var/lib/php/opcache-nginx ]; then
    mkdir /var/lib/php/opcache-nginx
    chgrp nginx /var/lib/php/opcache-nginx
    chmod 0770 /var/lib/php/opcache-nginx
fi

if [ ! -f /etc/php.d/gaia.ini ]; then
    ln -s "$RESDIR/php/gaia.ini" /etc/php.d/gaia.ini
fi
if [ -f /etc/php-fpm.d/www.conf ]; then
    echo "; Gaia override" >> /etc/php-fpm.d/www.conf
    echo "user = nginx" >> /etc/php-fpm.d/www.conf
    echo "group = nginx" >> /etc/php-fpm.d/www.conf
fi
service php-fpm start
chkconfig php-fpm on
service clamd start
chkconfig clamd on
