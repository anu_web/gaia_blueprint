#!/bin/bash

yum -y install MariaDB-Galera-server MariaDB-client galera rsync socat percona-xtrabackup

# Set up MariaDB.
if [ ! -f /etc/my.cnf.d/gaia.cnf ]; then
    ln -s "$RESDIR/mariadb/gaia.cnf" /etc/my.cnf.d/gaia.cnf
fi
if [ -f "$RESDIR/mariadb/gaia_$SERVER.cnf" ] && [ ! -f "/etc/my.cnf.d/gaia_$SERVER".cnf ]; then
    ln -s "$RESDIR/mariadb/gaia_$SERVER.cnf" "/etc/my.cnf.d/gaia_$SERVER.cnf"
fi
DB_DOMAIN_FILE="${DOMAIN//./_}.cnf"
if [ -f "$RESDIR/mariadb/$DOMAIN.cnf" ] && [ ! -f "/etc/my.cnf.d/$DB_DOMAIN_FILE" ]; then
    ln -s "$RESDIR/mariadb/$DOMAIN.cnf" "/etc/my.cnf.d/$DB_DOMAIN_FILE"
fi
if [ ! -d /var/log/mysql ]; then
    mkdir /var/log/mysql
    chown mysql:mysql /var/log/mysql
fi

# Set up SELinux.
setsebool -P rsync_client 1
semanage permissive -a rsync_t
semanage port -m -t mysqld_port_t -p tcp 4444
semanage port -a -t mysqld_port_t -p tcp 4567
semanage port -a -t mysqld_port_t -p tcp 4568
checkmodule -M -m -o /tmp/mariadb-galera.mod "$RESDIR/selinux/mariadb-galera.te"
semodule_package -o /tmp/mariadb-galera.pp -m /tmp/mariadb-galera.mod
semodule -i /tmp/mariadb-galera.pp
semanage permissive -a mysqld_t
