#!/bin/bash

AEGIR_ROOT_PASS=""
AEGIR_ROOT_PASS2=""
until [ -n "$AEGIR_ROOT_PASS" ] && [ "$AEGIR_ROOT_PASS" == "$AEGIR_ROOT_PASS2" ]; do
    read -s -p "Set password for the Aegir privileged user: " AEGIR_ROOT_PASS; echo
    if [ -z "$AEGIR_ROOT_PASS" ]; then
        echo 'The password must not be blank.'
        continue
    fi
    read -s -p "Enter password again to confirm: " AEGIR_ROOT_PASS2; echo
    if [ "$AEGIR_ROOT_PASS" != "$AEGIR_ROOT_PASS2" ]; then
        echo 'The entered passwords are not the same. Please try again.'
    fi
done
mysql -uroot -e"GRANT ALL PRIVILEGES ON *.* TO 'aegir_root'@'%' IDENTIFIED BY '$(printf "%q" "$AEGIR_ROOT_PASS")' WITH GRANT OPTION;"
AEGIR_ROOT_PASS2=""
