
import std;
include "gaia/legacy.vcl";
include "gaia/passive.vcl";

acl campus {
  "150.203";
  "130.56";
  "10";
}

acl essupport {
  "150.203.80";
  "10.100.85";
}

// TODO Update OneStop addresses with backend servers.
acl onestop {
  "101.178.172"; // onestopsecure.com / unilink.com.au
  "119.161.38"; // *.onestopsecure.com
}

sub vcl_recv {
  // Block non-standard methods.
  if (req.request != "GET" &&
    req.request != "HEAD" &&
    req.request != "PUT" &&
    req.request != "POST" &&
    req.request != "TRACE" &&
    req.request != "OPTIONS" &&
    req.request != "DELETE") {
      return (pipe);
  }

  // Set forwarding header.
  // This also prevents the user from spoofing a bogus header.
  set req.http.X-Forwarded-For = client.ip;

  // Block HTTPOXY CGI vulnerability.
  // https://httpoxy.org/
  //unset req.http.proxy;

  // Skip uncacheable methods.
  if (req.request != "GET" && req.request != "HEAD") {
    return (pass);
  }

  // Pass OneStop integration.
  if (client.ip ~ onestop) {
    return (pass);
  }

  // Restrict specific domain patterns to on campus access.
  if (req.http.host ~ "(-dev|-test)\.anu\.edu\.au$" && client.ip !~ campus) {
    error 403 "Access denied";
  }

  // Only trigger cookie requests with session IDs.
  if (!req.http.Cookie ~ "(^|;)\s*S?SESS[a-z0-9]+=") {
    remove req.http.Cookie;
  }

  // Skip install, update, and cron.
  if (req.url ~ "install\.php|update\.php|cron\.php") {
    return (pass);
  }

  // Skip authorised access.
  if (req.http.Authorization || req.url ~ "^/system/") {
    return (pass);
  }

  // Normalize the Accept-Encoding header.
  // http://varnish-cache.org/wiki/FAQ/Compression
  if (req.http.Accept-Encoding) {
    if (req.url ~ "\.(jpg|png|gif|gz|tgz|bz2|tbz|mp3|ogg)$") {
      remove req.http.Accept-Encoding;
    }
    elsif (req.http.Accept-Encoding ~ "gzip") {
      set req.http.Accept-Encoding = "gzip";
    }
    else {
      remove req.http.Accept-Encoding;
    }
  }

  // Set graceful cache.
  set req.grace = 30d;

  return (lookup);
}

sub vcl_hash {
  hash_data(req.url);
  if (req.http.host) {
    hash_data(req.http.host);
  }

  if (req.http.X-Forwarded-Proto) {
    hash_data(req.http.X-Forwarded-Proto);
  }

  return (hash);
}

sub vcl_fetch {
  // Prevent proxy-passed responses from being cached.
  if (beresp.http.X-Proxy-Nocache) {
    remove beresp.http.X-Proxy-Nocache;
    return (hit_for_pass);
  }

  // Remove extra Drupal and Aegir policy headers.
  remove beresp.http.X-Speed-Cache;
  remove beresp.http.X-Speed-Cache-UID;
  remove beresp.http.X-Speed-Cache-Key;
  remove beresp.http.X-NoCache;
  remove beresp.http.X-Force-Nocache;
  remove beresp.http.X-This-Proto;
  remove beresp.http.X-Server-Name;

  // Strip any cookies before an image/js/css is inserted into cache.
  if (req.url ~ "\.(png|gif|jpg|swf|css|js)$") {
    remove beresp.http.set-cookie;
  }
  set beresp.grace = 30d;

  // Force lookup for all requests.
  return (deliver);
}

sub vcl_deliver {
  if (req.http.X-Passive-Key) {
    set resp.http.X-Passive = "true";
  }

  // Remove system headers.
  unset resp.http.X-Generator;
  unset resp.http.X-Powered-By;
  unset resp.http.Server;
  unset resp.http.Via;

  if (client.ip ~ essupport) {
    if (obj.hits > 0) {
      set resp.http.X-Varnish-Cache = "HIT";
    }
    else {
      set resp.http.X-Varnish-Cache = "MISS";
    }
  }
  else {
    unset resp.http.Age;
    unset resp.http.X-Varnish;
    unset resp.http.X-Drupal-Cache;
  }
}

sub vcl_error {
  if (obj.status >= 500) {
    std.syslog(180, "Error (" + req.url + ") (" + obj.status + ") (" + obj.response + ") (" + req.xid + ")");
  }

  // Let's deliver a friendlier error page.
  // You can customize this as you wish.
  set obj.http.Content-Type = "text/html; charset=utf-8";
  synthetic {"
  <?xml version="1.0" encoding="utf-8"?>
  <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
   "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
  <html>
    <head>
      <title>"} + obj.status + " " + obj.response + {"</title>
      <style type="text/css">
      #page {width: 400px; padding: 10px; margin: 20px auto; border: 1px solid black; background-color: #FFF;}
      p {margin-left:20px;}
      body {background-color: #DDD; margin: auto;}
      </style>
    </head>
    <body>
    <div id="page">
    <h1>Page Could Not Be Loaded</h1>
    <p>We're very sorry, but the page could not be loaded properly. This should be fixed very soon, and we apologize for any inconvenience.</p>
    <hr />
    <h4>Debug Info:</h4>
    <pre>Status: "} + obj.status + {"
Response: "} + obj.response + {"
XID: "} + req.xid + {"</pre>
      </div>
    </body>
   </html>
  "};
  return (deliver);
}
