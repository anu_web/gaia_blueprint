
sub vcl_recv {
  // Proxy pass.
  if (req.http.host == "www.anu.edu.au" && req.url ~ "^/gms($|/)") {
    return (pass);
  }
  if (req.http.host == "shop.anu.edu.au" && req.url ~ "^/stockmgr/") {
    return (pass);
  }
}
