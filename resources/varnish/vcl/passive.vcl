
sub vcl_recv {
  // Mark as passive proxy.
  set req.http.X-Passive-Proxy = "true";

  if (req.restarts < 1 && req.http.X-Passive-Key) {
    remove req.http.X-Passive-Key;
  }

  if (req.http.X-Passive-Refresh) {
    return (pass);
  }
}

sub vcl_deliver {
  // Restart passive prompt.
  if (resp.status == 363) {
    if (resp.http.X-Passive-Request) {
      set req.http.X-Passive-Request = resp.http.X-Passive-Request;
    }
    if (resp.http.X-Passive-Key) {
      set req.http.X-Passive-Key = resp.http.X-Passive-Key;
    }
    return (restart);
  }

  // Clean up response header.
  if (resp.http.Vary ~ "(?i)X-Passive-Key") {
    // Remove Vary: X-Passive-Key to replace with Vary: Cookie.
    set resp.http.Vary = regsuball(resp.http.Vary, "(?i)(^|,\s*)X-Passive-Key,\s*", "\1");
    if (!resp.http.Vary ~ "(?i)Cookie") {
      set resp.http.Vary = "Cookie, " + resp.http.Vary;
    }
  }
}
