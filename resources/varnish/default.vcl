
backend gaia_web1 {
  .host = "gaia-web1.anu.edu.au";
  .port = "80";
  .probe = {
    .url = "/";
    .interval = 5s;
    .timeout = 10s;
    .window = 5;
    .threshold = 2;
  }
}

backend gaia_web2 {
  .host = "gaia-web2.anu.edu.au";
  .port = "80";
  .probe = {
    .url = "/";
    .interval = 5s;
    .timeout = 10s;
    .window = 5;
    .threshold = 2;
  }
}

director default_director round-robin {
  { .backend = gaia_web1; }
  { .backend = gaia_web2; }
}

sub vcl_recv {
  set req.backend = default_director;
}

include "gaia/gaia.vcl";
