
backend gaia_web1_dev {
  .host = "gaia-web1-dev.anu.edu.au";
  .port = "80";
  .probe = {
    .url = "/";
    .interval = 30s;
    .timeout = 10s;
    .window = 3;
    .threshold = 1;
  }
}

backend gaia_web2_dev {
  .host = "gaia-web2-dev.anu.edu.au";
  .port = "80";
  .probe = {
    .url = "/";
    .interval = 30s;
    .timeout = 10s;
    .window = 3;
    .threshold = 1;
  }
}

director dev_director round-robin {
  { .backend = gaia_web1_dev; }
  { .backend = gaia_web2_dev; }
}

sub vcl_recv {
  set req.backend = dev_director;
}

include "gaia/gaia.vcl";
